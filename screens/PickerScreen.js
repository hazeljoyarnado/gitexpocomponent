import React, { Component } from 'react';
import { Text, View, StyleSheet, Picker, Button } from 'react-native';

export default class PickerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      state: 'PHP'
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.paragraph}>
         Feel free to change the URL :)
        </Text>
        <Picker
          style={{ width: 100 }}
          selectedValue={this.state.language}
          onValueChange={(lang) => this.setState({ language: lang })}>
          <Picker.Item label="PHP" value="php" />
          <Picker.Item label="C++" value="c++" />
        </Picker>

        <Button
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
});
