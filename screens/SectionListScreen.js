import React, { Component } from 'react';
import { SectionList, Text, View, Button, StyleSheet } from 'react-native';

export default class SectionListScreen extends Component {
    render() {
        return (
            <View>
                <SectionList
                renderItem={({ item, index, section }) => <Text key={index}>{item}</Text>}
                renderSectionHeader={({ section: { title } }) => (
                    <Text style={{ fontWeight: 'bold' }}>{title}</Text>
                )}
                sections={[
                    { title: '1st Yr', data: ['IT1-A', 'IT1-B'] },
                    { title: '2nd Yr', data: ['IT2-A', 'IT2-B'] },
                    { title: '3rd Yr', data: ['IT3-A', 'IT3-B'] },
                ]}
                keyExtractor={(item, index) => item + index}
            />
            <Button style={styles.space}
                  title="Home"
                  onPress={() => this.props.navigation.navigate('Home')}
              />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    space: {
      margin: 20,
    }
  });