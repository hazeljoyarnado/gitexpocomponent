import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView, Button } from 'react-native';

export default class ScrollViewScreen extends Component {
   state = {
      names: [
         {'name': 'Hazel', 'id': 1},
         {'name': 'Shiela', 'id': 2},
         {'name': 'Joshua', 'id': 3},
         {'name': 'Chad', 'id': 4},
         {'name': 'Lea', 'id': 5},
         {'name': 'Regis', 'id': 6},
         {'name': 'Rosario', 'id': 7},
         {'name': 'Nard', 'id': 8},
         {'name': 'Aldave', 'id': 9},
         {'name': 'Jovelyn', 'id': 10},
         {'name': 'Alber', 'id': 11},
         {'name': 'Arnado', 'id': 12}
      ]
   }
   render() {
      return (
         <View>
            <ScrollView>
               {
                  this.state.names.map((item, index) => (
                     <View key = {item.id} style = {styles.item}>
                        <Text>{item.name}</Text>
                     </View>
                  ))
               }
               <Button
                    title="Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
            </ScrollView>
            
         </View>
      )
   }
}

const styles = StyleSheet.create ({
   item: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: 30,
      margin: 2,
      borderColor: '#2a4944',
      borderWidth: 1,
      backgroundColor: '#d2f7f1'
   }
});